import os
import json
import logging
from zeebe import ZeebeWorker

logging.basicConfig(level=os.environ.get("LOGLEVEL", "INFO"))

ZEEBE_ADDRESS = os.environ.get("ZEEBE_ADDRESS", "zeebe:26500")
ZEEBE_WORKER_NAME = os.environ.get("ZEEBE_WORKER_NAME", "Zeebe Echo Worker")


def echo(job):
    logging.info(f"Worker: {ZEEBE_WORKER_NAME}")
    logging.info("Task: Echo")

    variables = json.loads(job.variables)
    custom_headers = json.loads(job.customHeaders)

    logging.info(f"Job: {job}")
    logging.info(f"Variables: {variables}")
    logging.info(f"Custom Headers: {custom_headers}")


def main():
    zw = ZeebeWorker(
        zeebe_address=ZEEBE_ADDRESS,
        name=ZEEBE_WORKER_NAME,
    )

    zw.register_task(task_type="echo", function=echo)

    zw.run()


if __name__ == "__main__":
    main()
